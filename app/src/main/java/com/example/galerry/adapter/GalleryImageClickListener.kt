package com.example.galerry.adapter

interface GalleryImageClickListener {
    fun onClick(position: Int)
    //The purpose of the listener code above is to be used by the adapter to handle user clicking the image
}