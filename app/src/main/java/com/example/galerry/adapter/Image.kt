package com.example.galerry.adapter

data class Image (
    val imageUrl: String,
    val title: String
)